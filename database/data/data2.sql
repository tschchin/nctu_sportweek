use final;

INSERT INTO department (id,name,class, grade)
VALUES (0,'cs','A',1);

INSERT INTO department (id,name,class, grade)
VALUES (0,'cs','B',1);

INSERT INTO department (id,name,class, grade)
VALUES (0,'cs','A',2);

INSERT INTO department (id,name,class, grade)
VALUES (0,'cs','B',2);

INSERT INTO department (id,name,class, grade)
VALUES (0,'ece','A',1);

INSERT INTO department (id,name,class, grade)
VALUES (0,'ece','B',1);

INSERT INTO department (id,name,class, grade)
VALUES (0,'ece','A',2);

INSERT INTO department (id,name,class, grade)
VALUES (0,'ece','B',2);
