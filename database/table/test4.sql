use final;

CREATE TABLE registration
(
	id INTEGER AUTO_INCREMENT,
	event_id INTEGER,
	team_id INTEGER,
	time DATETIME NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (event_id) REFERENCES event(id),
	FOREIGN KEY (team_id) REFERENCES team(team_id),
	CONSTRAINT U_registration UNIQUE(event_id,team_id)
);
