use final;

CREATE TABLE team
(
	team_id INTEGER,
	user_id INTEGER,
	name VARCHAR(64) NOT NULL,
	CONSTRAINT id_pair PRIMARY KEY (team_id,user_id),
	CONSTRAINT U_team UNIQUE (team_id,user_id,name),
	FOREIGN KEY (user_id) REFERENCES user_table(user_id)
);

