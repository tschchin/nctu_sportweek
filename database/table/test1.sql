use final;

CREATE TABLE department
(
	id INTEGER AUTO_INCREMENT,
	name VARCHAR(64),
	class CHAR(1),
	grade INTEGER,
	PRIMARY KEY (id),
	CONSTRAINT U_department UNIQUE (name,class,grade)
);

CREATE TABLE event
(
	id INTEGER AUTO_INCREMENT,
	team_limit INTEGER,
	name VARCHAR(64),
	max_team_members INTEGER,
	min_team_members INTEGER,
	ddate DATE NOT NULL,
	is_delete BOOLEAN,
	rule VARCHAR(256),
	PRIMARY KEY (id),
	UNIQUE (name)
);

CREATE TABLE match_table
(
	id INTEGER AUTO_INCREMENT,
	score INTEGER,
	datetime DATETIME NOT NULL,
	valid TINYINT(1) NOT NULL,
	order1 INTEGER NOT NULL,
	team_id INTEGER NOT NULL,
	event_id INTEGER NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT U_match UNIQUE (order1,team_id,event_id)
);
