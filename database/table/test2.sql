use final;

CREATE TABLE user_table
(
	user_id INTEGER,
	email VARCHAR(128),
	gender CHAR(1),
	phone_number INTEGER,
	department_id INTEGER,
	name VARCHAR(64),
	identity VARCHAR(64),
	passward VARCHAR(64),
	PRIMARY KEY (user_id),
	FOREIGN KEY (department_id) REFERENCES department(id)
);

