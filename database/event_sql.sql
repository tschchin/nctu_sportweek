create table event
{
	id INTEGER AUTO_INCREMENT,
	tesm_limit INTEGER,
	name varchar(64),
	max_team_limit INTEGER,
	min_team_limit INTEGER,
	year YEAR(4) NOT NULL,
	is_delete BOOLEAN NOT NULL,
	PRIMARY KEY(id),
	UNIQUE (name)
}
