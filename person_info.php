<!doctype html>
<html>
	<?php include "sql.php"?>
	<?php session_start();
		$account = $_SESSION['account'];
    if(empty($account)){
      echo "<script>
              alert('尚未登入');
              window.location.href='home.php';
            </script>";
    }
  ?>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="final of database">
		<meta name="author" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>個人資料</title>

		<!-- Bootstrap core CSS -->

		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">-->
		<link rel="stylesheet" href="./bootstrap-4.1.1/dist/css/bootstrap.min.css">

		<link rel="stylesheet" href="./css/home.css">
		<link rel="stylesheet" href="./css/session.css">
		<link rel="stylesheet" href="./css/announce.css">
		<!-- Custom styles for this template -->
	</head>

	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark">
				<div class="container-fluid">
					<a class="navbar-brand" href="#">N C T U &nbsp;&nbsp; S p o r t s</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="nav navbar-nav mr-auto">
							<li class="nav-item">
								<a class="nav-link" href="./home.php">首頁 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="./game_match.php">賽程 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="./events.php">活動列表 <span class="sr-only">(current)</span></a>
							</li>
							<?php
							$account = $_SESSION['account'];
              $identity = $_SESSION['identity'];
							/*---------登入後---------*/
								if ($account != null)
								{
                  if($identity == "admin")
                  {?>
                    <li class="nav-item active">
      								<a class="nav-link" href="./member_manage.php">成員管理 <span class="sr-only">(current)</span></a>
      							</li>
	                <?php
	                }
									//取出user的name
									$sql = "SELECT name FROM User where user_id = '$account'";
									$result = mysqli_query($conn,$sql);
									$row = @mysqli_fetch_row($result);
									?>
									<li class="nav-item active">
										<a class="nav-link" href="#">Hi, <?php echo $row[0] ?></a>
										<ul class="profile-option">
											<li><a class="nav-link active" href="./person_info.php">個人資料</a></li>
											<li><a href="./logout.php">登出</a></li>
										</ul>
									</li>
								<?php
								}
								/*---------未登入---------*/
								else {?>
									<li class="nav-item active">
										<a class="nav-link " href="./register.php">註冊 <span class="sr-only">(current)</span></a>
									</li>
									<li class="nav-item active">
										<a class="nav-link " href="./login.php">登入 <span class="sr-only">(current)</span></a>
									</li>
								<?php
								}?>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<div class="container home">
			<div class="content anncs_list">
				<div class="head">個人資料</div><br><br>
			<?php
				/*if ($_SESSION['identity']!='normal'){
					echo '<br>';
					echo '<a href="person/edit_person_info.php"><button class="btn btn-primary" style="float:right;">修改資料</button></a>';
					echo '<br>';
					echo '<br>';
				}*/
			?>
			<table class="table text-center">
				<?php
          $sql = "SELECT * FROM User WHERE user_id = '$account'";
          $result = $conn->query($sql);
          $row = $result->fetch_assoc();

          $dep_id = $row['department_id'];
          $dep_sql = "SELECT * FROM Department WHERE id = '$dep_id'";
          $dep_result = $conn->query($dep_sql);
          $dep_row = $dep_result->fetch_assoc();

          $gender = "";
          if($row['gender'] == 0) $gender = "男";
          else $gender = "女";

					echo '<tr>';
					echo '<th class="text-center">學號</th>';
					echo '<td class="text-center">'.$row['user_id'].'</th>';
					echo '</tr>';
          echo '<tr>';
					echo '<th class="text-center">姓名</th>';
					echo '<td class="text-center">'.$row['name'].'</th>';
					echo '</tr>';
          echo '<tr>';
					echo '<th class="text-center">性別</th>';
					echo '<td class="text-center">'.$gender.'</th>';
					echo '</tr>';
          echo '<tr>';
					echo '<th class="text-center">系所</th>';
					echo '<td class="text-center">'.$dep_row['name'].' '.$dep_row['grade'].'年級 '.$dep_row['class'].'班</th>';
					echo '</tr>';
          echo '<tr>';
					echo '<th class="text-center">信箱</th>';
					echo '<td class="text-center">'.$row['email'].'</th>';
					echo '</tr>';
          echo '<tr>';
					echo '<th class="text-center">電話</th>';
					echo '<td class="text-center">'.$row['phone_number'].'</th>';
					echo '</tr>';




				?>
			</table>
			</div>
		</div>


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    	<script>window.jQuery || document.write('<script src="./bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
		<script src="./bootstrap-4.1.1/assets/js/vendor/holder.min.js"></script>
		<script src="./bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
		<script src="./bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
	</body>
</html>
