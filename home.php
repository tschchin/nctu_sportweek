<!doctype html>
<html>
	<?php include "sql.php"?>
	<?php session_start(); ?>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="final of database">
		<meta name="author" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>交大體育週</title>

		<!-- Bootstrap core CSS -->

		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">-->
		<link rel="stylesheet" href="./bootstrap-4.1.1/dist/css/bootstrap.min.css">

		<link rel="stylesheet" href="./css/home.css">
		<link rel="stylesheet" href="./css/session.css">
		<link rel="stylesheet" href="./css/announce.css">
		<!-- Custom styles for this template -->
	</head>

	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark">
				<div class="container-fluid">
					<a class="navbar-brand" href="#">N C T U &nbsp;&nbsp; S p o r t s</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="nav navbar-nav mr-auto">
							<li class="nav-item active">
								<a class="nav-link active" href="home.php">首頁 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="game_match.php">賽程 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link " href="events.php">活動列表 <span class="sr-only">(current)</span></a>
							</li>

							<?php
							$account = $_SESSION['account'];
							$identity = $_SESSION['identity'];
							/*---------登入後---------*/
								if ($account != null)
								{
									if($identity == "admin") {?>
                    				<li class="nav-item active">
      										<a class="nav-link" href="./member_manage.php">成員管理 <span class="sr-only">(current)</span></a>
      								</li>
	               			 <?php }
									//取出user的name
									$sql = "SELECT name FROM User where user_id = '$account'";
									$result = mysqli_query($conn,$sql);
									$row = @mysqli_fetch_row($result);
									?>
									<li class="nav-item active">
										<a class="nav-link" href="#">Hi, <?php echo $row[0] ?></a>
										<ul class="profile-option">
											<li><a href="person_info.php">個人資料</a></li>
											<li><a href="logout.php">登出</a></li>
										</ul>
									</li>
								<?php
								}
								/*---------未登入---------*/
								else {?>
									<li class="nav-item active">
										<a class="nav-link " href="register.php">註冊 <span class="sr-only">(current)</span></a>
									</li>
									<li class="nav-item active">
										<a class="nav-link " href="login.php">登入 <span class="sr-only">(current)</span></a>
									</li>
								<?php
								}?>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<div class="container home">
			<div class="content anncs_list">
				<div class="head">公告</div>
				<br>
			<?php
				if ($_SESSION['identity']=='admin'){
					echo '<a href="anncs/add.php"><button class="btn btn-primary" style="float:right;">新增公告</button></a>';
					echo '<br>';
					echo '<br>';
				}
			?>
			<table class="table text-center">
				<?php
					echo '<tr>';
					echo '<th class="text-center">日期</th>';
					echo '<th class="text-center">名稱</th>';
					if ( $_SESSION['identity'] == 'admin') {
						echo '<th class="text-center">編輯</th>';
						echo '<th class="text-center">發文者</th>';
					}
					echo '</tr>';
					$sql = "SELECT title,date(date) as date,id,name FROM Anncs inner join User ON editor=user_id";
					$result = $conn->query($sql);
					if ($result->num_rows > 0 ) {
						// output data of each row
						while($row = $result->fetch_assoc()) {
							echo '<tr>';
							echo '<td>'.$row['date'].'</td>';
							echo '<td><a href=anncs.php?id='.$row['id'].'>'.$row['title'].'</a></td>';
							#echo '<td>'.$identity[0].'</td>';
							if ( $_SESSION['identity']=='admin'){
								echo '<td>';
								echo '<a href=anncs/edit.php?id='.$row['id'].'>';
								echo '<button class="btn btn-editor"style="cursor:pointer" >編輯</button></a>';
								echo '<button class="btn btn-del" name="del" value='.$row['id'].' style="cursor:pointer">刪除</button></td>';
								echo '<td>'.$row['name'].'</td>';
							}
							echo '</tr>';
						}
					}
					else {
						echo "0 results";
					}
				?>
			</table>
			</div>
		</div>


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    	<script>window.jQuery || document.write('<script src="./bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
		<script src="./bootstrap-4.1.1/assets/js/vendor/holder.min.js"></script>
		<script src="./bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
		<script src="./bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script>
		    $("button[name='del']").click(function(){
		      id =  $(this).val();
		      console.log(id);
			  if(confirm('確定刪除？')) {
		          $.post("./anncs/delete.php",
		          {
		            id: id,
		          },
		          function(data){
		              alert(data);
		              location.reload();
		          });
		        }
			});
		 </script>
	</body>
</html>
