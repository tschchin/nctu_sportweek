<!doctype html>
<?php session_start(); ?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="final of database">
		<meta name="author" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>註冊</title>

		<!-- Bootstrap core CSS -->

		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">-->
		<link rel="stylesheet" href="./bootstrap-4.1.1/dist/css/bootstrap.min.css">

		<link rel="stylesheet" href="./css/home.css">
		<link rel="stylesheet" href="./css/album.css">
		<link rel="stylesheet" href="./css/register.css">

		<!-- Custom styles for this template -->
	</head>

	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark">
				<div class="container-fluid">
					<a class="navbar-brand" href="#">N C T U &nbsp;&nbsp; S p o r t s</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="nav navbar-nav mr-auto">
							<li class="nav-item active">
								<a class="nav-link" href="home.php">首頁 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="game_match.php">賽程 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="events.php">活動列表 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link active" href="./register.php">註冊 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link " href="login.php">登入 <span class="sr-only">(current)</span></a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>

<?php
	include("sql.php");

	$noInfo_flag = false;
	$duplicate_flag = false;
	$differ_password = false;
	$wrong_email = false;
	$success_flag = false;
	$fail_flag = false;
	$check_authcode = false;

	error_reporting(0);

	//送出鍵按出後
	if(isset($_GET['submit'])==true){
	 //檢査所有欄位有沒有輸入
	 if(empty($_GET['email'])==true || empty($_GET['password'])==true ||
	 		empty($_GET['name'])==true || empty($_GET['account'])==true ||
			empty($_GET['confirm_password'])==true || empty($_GET['phone'])==true ||
			empty($_GET['gender'])==true || empty($_GET['department'])==true ||
			empty($_GET['grade'])==true || empty($_GET['class'])==true || empty($_GET['authcode'])){
	 //有缺的話，叫使用者寫完
		 $noInfo_flag = true;
		}
		elseif(empty($_GET['email'])==false && empty($_GET['password'])==false &&
			 empty($_GET['name'])==false && empty($_GET['account'])==false &&
			 empty($_GET['confirm_password'])==false && empty($_GET['phone'])==false &&
			 empty($_GET['gender'])==false && empty($_GET['department'])==false &&
			 empty($_GET['grade'])==false && empty($_GET['class'])==false && empty($_GET['authcode'])==false)
		{
			$account = $_GET['account'];
			$password = $_GET['password'];
			$confirm_password = $_GET['confirm_password'];
			$authcode = $_GET['authcode'];

			$sql = "SELECT user_id FROM User where user_id = '$account'";
			$result = mysqli_query($conn,$sql);
			$check_account = @mysqli_fetch_row($result);
	 		if($check_account[0] == $_GET['account']){
	 			//重覆到的話，退回
	 			$duplicate_flag = true;
			}
			elseif($password != $confirm_password)
			{
				//密碼不一致
				$differ_password = true;
			}
			elseif(!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $_GET["email"]))
			{
				//信箱格式錯誤
				$wrong_email = true;
			}
			elseif(strtolower($authcode) != $_SESSION['authcode'])
			{
				//驗證碼錯誤
				$check_authcode = true;
			}
			else{
			 //完全正確，寫入資料
			 $password = md5($_GET['password']);
			 if($_GET['gender'] == "male")
			 {
				 $gender = 0;
			 }
			 elseif($_GET['gender'] == "female")
			 {
				 $gender = 1;
			 }
			 $depart_name = $_GET['department'];
			 $depart_grade = $_GET['grade'];
			 $depart_class = $_GET['class'];
			 $sql = "SELECT id FROM Department where name = '$depart_name' and class = '$depart_class' and grade = '$depart_grade'";
			 $result = mysqli_query($conn,$sql);
			 $department = @mysqli_fetch_row($result);

			 $insert_sql = "INSERT INTO User (user_id, password, name, email, gender, phone_number, identity, department_id)
											VALUES('$_GET[account]','$password','$_GET[name]','$_GET[email]','$gender','$_GET[phone]','normal', '$department[0]')";
			 $insert = mysqli_query($conn, $insert_sql);
			 if(!$insert){
				 $fail_flag = true;
			 }
			 else{
				 $success_flag = true;
			 }
		 }
		}
	}
?>
	<?php if($noInfo_flag){ ?>
	 <div class="alert alert-danger alert-dismissible" role="alert">
	 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			請輸入所有欄位！
	 </div>
	<?php }?>
	<?php if($duplicate_flag){ ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			此學號已經註冊過！
		</div>
	<?php }?>
	<?php if($differ_password){ ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			密碼與確認密碼不相同
		</div>
	<?php }?>
	<?php if($wrong_email){ ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			信箱格式錯誤
		</div>
	<?php }?>
	<?php if($check_authcode){ ?>
	 <div class="alert alert-danger alert-dismissible" role="alert">
	 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	 		驗證碼錯誤！
	 </div>
	<?php }?>
	<?php if($success_flag){ ?>
		<!--<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			註冊成功！
		</div>-->
					<script>
						alert('註冊成功');
						window.location.href='home.php';
					</script>
	<?php }?>
	<?php if($fail_flag){ ?>
	 <div class="alert alert-danger alert-dismissible" role="alert">
	 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	 		註冊失敗！
	 </div>
	<?php }?>

		<div class="container register-wrapper">
			<h1 style="text-align:center;">註冊</h1><br>

		  <div id="center_block">
		 	 <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="GET" style="text-align:center">
		 	 <input type="text" placeholder="學號" name="account" id="text" value=""><br><br>
		 	 <input type="password" placeholder="密碼" name="password" ><br><br>
		 	 <input type="password" placeholder="確認密碼" name="confirm_password" ><br><br>
		 	 <input type="text" placeholder="姓名" name="name" id="text" value=""><br><br>
		 	 性別
		 	 <input type="radio" name="gender" value="male" checked="true">男
		 	 <input type="radio" name="gender" value="female">女<br><br>
		 	 <input type="text" placeholder="系所" name="department" id="text" value=""><br><br>
			 <select name = "grade">
	      <option value ="1">一</option>
	      <option value ="2">二</option>
	      <option value="3">三</option>
	      <option value="4">四</option>
	      <option value="5">五</option>
	      <option value="6">六</option>
	      <option value="7">七</option>
	     </select>年級
			 <select name = "class">
	      <option value ="A">A</option>
	      <option value ="B">B</option>
	      <option value="C">C</option>
	      <option value="D">D</option>
			</select>班<br><br>
		 	 <input type="text" placeholder="信箱" name="email" id="text" value=""><br><br>
		 	 <input type="text" placeholder="電話" name="phone" id="text" value=""><br><br>
			 <br><br>

			 <p>驗證碼： <img id="captcha_img" border='1' src='./captcha.php?r=echo rand(); ?>' style="width:100px; height:30px">
			 <a href="javascript:void(0)" onclick="document.getElementById('captcha_img').src='captcha.php?r='+Math.random()">換一個?</a>
			 <br><br>
			 <input type="text" placeholder="請輸入驗證碼" name='authcode' value=''>

			 <br><br>
		 	 <input type="submit" name="submit" value="註冊">
			 <input type="button" name="cancel" value="取消" onclick="window.location.href ='login.php'">

			 <br><br>
		 	 </form>
		  </div>
		</div>



		<footer class="text-muted">
			<div class="container">
				<p class="float-right">
					<a href="#">Back to top</a>
				</p>
			</div>
		</footer>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    	<script>window.jQuery || document.write('<script src="./bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
		<script src="./bootstrap-4.1.1/assets/js/vendor/holder.min.js"></script>
		<script src="./bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
		<script src="./bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
	</body>
</html>
