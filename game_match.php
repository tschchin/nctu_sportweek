<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="final of database">
		<meta name="author" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>game_match</title>

		<!-- Bootstrap core CSS -->

		<link rel="stylesheet" href="./bootstrap-4.1.1/dist/css/bootstrap.min.css">

		<link rel="stylesheet" href="./css/home.css">
		<link rel="stylesheet" href="./css/event.css">
		<link rel="stylesheet" href="./css/session.css">

		<!-- Custom styles for this template -->
		<?php
			include "sql.php";
			session_start();
			if($_SESSION['url'] != ($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']) ) 
				$_SESSION['url'] = ($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		?>
	</head>

	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark">
				<div class="container-fluid">
					<a class="navbar-brand" href="./home.php">N C T U &nbsp;&nbsp; S p o r t s</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="nav navbar-nav mr-auto">
							<li class="nav-item">
								<a class="nav-link" href="./home.php">首頁 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link active" href="./game_match.php">賽程 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="./events.php">活動列表 <span class="sr-only">(current)</span></a>
							</li>
							<?php
							$account = $_SESSION['account'];
              $identity = $_SESSION['identity'];
							/*---------登入後---------*/
								if ($account != null)
								{
                  if($identity == "admin")
                  {?>
                    <li class="nav-item active">
      								<a class="nav-link" href="./member_manage.php">成員管理 <span class="sr-only">(current)</span></a>
      							</li>
	                <?php
	                }
									//取出user的name
									$sql = "SELECT name FROM User where user_id = '$account'";
									$result = mysqli_query($conn,$sql);
									$row = @mysqli_fetch_row($result);
									?>
									<li class="nav-item active">
										<a class="nav-link" href="#">Hi, <?php echo $row[0] ?></a>
										<ul class="profile-option">
											<li><a href="./person_info.php">個人資料</a></li>
											<li><a href="./logout.php">登出</a></li>
										</ul>
									</li>
								<?php
								}
								/*---------未登入---------*/
								else {?>
									<li class="nav-item active">
										<a class="nav-link " href="./register.php">註冊 <span class="sr-only">(current)</span></a>
									</li>
									<li class="nav-item active">
										<a class="nav-link " href="./login.php">登入 <span class="sr-only">(current)</span></a>
									</li>
								<?php
								}?>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<div class="container">
			<h3 class="title" style="text-align:center;">活動列表</h3>
			<br>
			<br>
			<br>

			<div class="album py-10 bg-light">
				<div class="row">

					<?php
						$query = "SELECT id , name , rule , ddate from event where is_delete";
						$result = $conn->query($query);

						if ($result->num_rows > 0 )
						{
							while($row = $result->fetch_assoc())
							{
								printf("<div class='col-md-4'>\n");
								printf("<div class='card mb-4 box-shadow'>\n");
								printf("<img class='card-img-top' data-src='holder.js/100px100?theme=thumb&bg=55595c&fg=eceeef&text= %s ' alt='Card image cap'>\n",$row['name']);
								printf("<div class='card-body'>\n");
								printf("<p class='card-text' style='text-align: center;'> %s.<br></p>\n",$row['rule']);
								printf("<p class='card-text' style='text-align: right;'> %s.<br></p>\n",$row['ddate']);
								printf("<div class='d-flex justify-content-between align-items-center event-wrapper '>\n");

								

								printf("<div class='btn-group' style='  float:right; ' >\n");

								printf("<a href='./match.php?id=%d'><button class='btn btn-situation '>賽程</button></a>\n",$row['id']);
								/*
								$sql = "SELECT * FROM (Registration as G INNER JOIN team_information as TI ON G.team_id = TI.team_id) where event_id = ".$id;
								*/
								printf("</div>\n");	
								printf("</div>\n");
								printf("</div>\n");
								printf("</div>\n");
								printf("</div>\n");

							}
						}
						$result->close();
					?>

				</div>
			</div>
		</div>




		<footer class="text-muted">
			<div class="container">
				<p class="float-right">
					<a href="#">Back to top</a>
				</p>
			</div>
		</footer>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    	<script>window.jQuery || document.write('<script src="./bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
		<script src="./bootstrap-4.1.1/assets/js/vendor/holder.min.js"></script>
		<script src="./bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
		<script src="./bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
	</body>
</html>
