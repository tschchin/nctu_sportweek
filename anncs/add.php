<!doctype html>
<html>
	<?php include "../sql.php"?>
	<?php session_start(); ?>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="final of database">
		<meta name="author" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>新增公告</title>

		<!-- Bootstrap core CSS -->

		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">-->
		<link rel="stylesheet" href="../bootstrap-4.1.1/dist/css/bootstrap.min.css">

		<link rel="stylesheet" href="../css/home.css">
		<link rel="stylesheet" href="../css/session.css">
		<link rel="stylesheet" href="../css/announce.css">
		<!-- Custom styles for this template -->
	</head>

	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark">
				<div class="container-fluid">
					<a class="navbar-brand" href="#">N C T U &nbsp;&nbsp; S p o r t s</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="nav navbar-nav mr-auto">
							<li class="nav-item active">
								<a class="nav-link active" href="../home.php">首頁 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link " href="../game_match.php">賽程 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link " href="../events.php">活動列表 <span class="sr-only">(current)</span></a>
							</li>
							<?php
							$account = $_SESSION['account'];
				              $identity = $_SESSION['identity'];
				              if($identity != "admin"){
				              	echo "<script>alert('禁止通行呦～～')</script>";
				              	echo "<script>window.location.href='../home.php'</script>";
				              	

				              }
							/*---------登入後---------*/
								if ($account != null)
								{
                  if($identity == "admin")
                  {
                    echo '<li class="nav-item active">';
      							echo '<a class="nav-link" href="./member_manage.php">成員管理 <span class="sr-only">(current)</span></a>';
      							echo '</li>';

                  }
									//取出user的name
									$sql = "SELECT name FROM User where user_id = '$account'";
									$result = mysqli_query($conn,$sql);
									$row = @mysqli_fetch_row($result);
									?>
									<li class="nav-item active">
										<a class="nav-link" href="#">Hi, <?php echo $row[0] ?></a>
										<ul class="profile-option">
											<li><a href="./person_info.php">個人資料</a></li>
											<li><a href="./logout.php">登出</a></li>
										</ul>
									</li>
								<?php
								}
								/*---------未登入---------*/
								else {?>
									<li class="nav-item active">
										<a class="nav-link " href="./register.php">註冊 <span class="sr-only">(current)</span></a>
									</li>
									<li class="nav-item active">
										<a class="nav-link " href="./login.php">登入 <span class="sr-only">(current)</span></a>
									</li>
								<?php
								}?>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<div class="container home">
			<div class="head">新增公告</div>
				<br>
			 <?php

			    $title = $editor = $content = $type =  "";
			    if( isset($_POST[content]) && isset($_POST[title]) ){
			      $title = $_POST[title];
			      $editor = $_SESSION['account'];
			      $content = $_POST[content];
			      $type = $_POST[type];
			      $sql = "INSERT INTO Anncs (top,title,editor,content) VALUES($type,'$title','$editor','$content')";
			      $result = mysqli_query($conn,$sql);
			      header("Location: ../home.php");
			    }
			  ?>
			  <div class="add_anncs_form">
			    <form style="text-align: center" autocomplete="off" method="post" action="./add.php">
			      標題: <input type="text" name="title" value="<?php echo $title;?>">
			      <br><br>
			      內容: <br>
			      <textarea name="content" style="resize: none; width:500px; height:100px;"></textarea>
			      <br><br>
			      分類:
			      <input type="radio" name="type" <?php if (isset($type) && $type=="置頂") echo "checked";?> value="1">置頂
			      <input type="radio" name="type" <?php if (isset($type) && $type=="一般") echo "checked";?> value="0">一般
			      <br><br>
			      <input type="submit" name="submit" value="發布">
			      <input type="button" name="cancel" value="取消" onclick="window.location.href ='../home.php'">

			    </form>
			  </div>
		</div>


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    	<script>window.jQuery || document.write('<script src="./bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
		<script src="../bootstrap-4.1.1/assets/js/vendor/holder.min.js"></script>
		<script src="../bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
		<script src="../bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
	</body>
</html>
