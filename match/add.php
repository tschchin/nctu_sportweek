<!doctype html>
<html>
	<?php include "../sql.php"?>
	<?php session_start(); 
  if ($_SESSION['identity']!='admin'){
  	echo "<script>alert('禁止通行呦～～')</script>";
	echo "<script>window.location.href='../home.php'</script>";
  }
?>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="final of database">
		<meta name="author" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>交大體育週</title>

		<!-- Bootstrap core CSS -->

		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">-->
		<link rel="stylesheet" href="../bootstrap-4.1.1/dist/css/bootstrap.min.css">

		<link rel="stylesheet" href="../css/home.css">
		<link rel="stylesheet" href="../css/session.css">
		<link rel="stylesheet" href="../css/announce.css">
		<!-- Custom styles for this template -->
	</head>
	<style type="text/css">
		.team_1 {
			display: none;
		}
		.team_2 {
			display: none;
		}
	</style>

	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark">
				<div class="container-fluid">
					<a class="navbar-brand" href="#">N C T U &nbsp;&nbsp; S p o r t s</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="nav navbar-nav mr-auto">
							<li class="nav-item active">
								<a class="nav-link " href="../home.php">首頁 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link active" href="../game_match.php">賽程 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link " href="../events.php">活動列表 <span class="sr-only">(current)</span></a>
							</li>
							<?php
							$account = $_SESSION['account'];
              $identity = $_SESSION['identity'];
							/*---------登入後---------*/
								if ($account != null)
								{
                  if($identity == "admin")
                  {
                    echo '<li class="nav-item active">';
      							echo '<a class="nav-link" href="../member_manage.php">成員管理 <span class="sr-only">(current)</span></a>';
      							echo '</li>';

                  }
									//取出user的name
									$sql = "SELECT name FROM User where user_id = '$account'";
									$result = mysqli_query($conn,$sql);
									$row = @mysqli_fetch_row($result);
									?>
									<li class="nav-item active">
										<a class="nav-link" href="#">Hi, <?php echo $row[0] ?></a>
										<ul class="profile-option">
											<li><a href="./person_info.php">個人資料</a></li>
											<li><a href="./logout.php">登出</a></li>
										</ul>
									</li>
								<?php
								}
								/*---------未登入---------*/
								else {?>
									<li class="nav-item active">
										<a class="nav-link " href="./register.php">註冊 <span class="sr-only">(current)</span></a>
									</li>
									<li class="nav-item active">
										<a class="nav-link " href="./login.php">登入 <span class="sr-only">(current)</span></a>
									</li>
								<?php
								}?>
						</ul>
					</div>
				</div>
			</nav>
		</header>
<!-- Check format is right-->
<?php
	$name = false;
	$team_1 = false;
	$time = false;
	$id = $_GET[id];
	error_reporting(0);
	if(isset($_POST[submit])) {
		if($_POST[type]==1){
			$sql = "INSERT INTO Game_match (event_id,team_id,valid,datetime) VALUES($_GET[id],$_POST[team_1],1,'$_POST[time]')";
			$result = mysqli_query($conn,$sql);
			#header("Location: ../home.php");
		}
		if($_POST[type]==2){
			if ($_POST[team_1]!=0 && $_POST[team_2]!=0){
				$sql = "INSERT INTO Game_match (event_id,team_id,valid,datetime) VALUES($_GET[id],$_POST[team_1],1,'$_POST[time]')";
				$result = mysqli_query($conn,$sql);
				$sql = "INSERT INTO Game_match (event_id,team_id,valid,datetime) VALUES($_GET[id],$_POST[team_2],1,'$_POST[time]')";
				$result = mysqli_query($conn,$sql);
			}
			else{
				echo '<script>alert("請填入隊伍！")</script>';
				#echo htmlspecialchars($_SERVER["PHP_SELF"].'?id='.$_GET[id]);
			}
		
		}
	}
?>

		<div class="container home">
			  <div class="add_anncs_form">
			    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"].'?id='.$_GET[id]);?>" method="POST" style="text-align:center">
			      賽制：
			      <select name="type" id="game_type">
			          <option value="0">請選擇</option>
					  <option value="1">成績競賽</option>
					  <option value="2">對抗賽</option>
				  </select>
				  <br>
				  <p  class=team_1 style="display: none;">隊伍1</p>
				  <?php
					echo '<select name="team_1" class=team_1 id="team">';
					echo '<option value=0>請選擇</option>';
					$sql = "SELECT DISTINCT name,R.team_id FROM team_information as T inner join Registration as R ON T.team_id=R.team_id WHERE event_id = $_GET[id]";
					$result = $conn->query($sql);
					while($row = $result->fetch_assoc()) {
						$name = '';
						$temp = explode("_",$row['name']);
						for($i=0;$i<count($temp)-1;$i++)
							$name .= $temp[$i];
						echo '<option value='.$row['team_id'].'>'.$name.'</option>';
					}
					echo '</select>';
				  ?>
				  <br>
				   <p  class=team_2 style="display: none;">隊伍2</p>
				  <?php
					echo '<select name="team_2" class=team_2 id="team">';
					echo '<option value=0>請選擇</option>';
					$sql = "SELECT DISTINCT name,R.team_id FROM team_information as T inner join Registration as R ON T.team_id=R.team_id WHERE event_id = $_GET[id]";
					$result = $conn->query($sql);
					while($row = $result->fetch_assoc()) {
						$name = '';
						$temp = explode("_",$row['name']);
						for($i=0;$i<count($temp)-1;$i++)
							$name .= $temp[$i];
						echo '<option value='.$row['team_id'].'>'.$name.'</option>';
					}
					echo '</select>';
				  ?>
				  <br>
				  <p class=team_1>時間</p>
				  <input  name="time" class=team_1 type="datetime-local" id="appt-time" name="appt-time" required />
				  <br>
			      <input type="submit" name="submit" value="發布">
			      <input type="button" name="cancel" value="取消" onclick="window.location.href ='../game_match.php'">

			    </form>
			  </div>
		</div>


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    	<script>window.jQuery || document.write('<script src="./bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
		<script src="../bootstrap-4.1.1/assets/js/vendor/holder.min.js"></script>
		<script src="../bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
		<script src="../bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script>
			$('#game_type').on('change', function() {
				type = this.value;
				if (type == 0){
					$(".team_1").css('display','none');
					$(".team_2").css('display','none');
			        alert("請先選擇賽制");
			    }
			    else if (type == 1){
			    	$(".team_2").css('display','none');
			    	$(".team_1").css('display','inline');
			    }
			    else if (type == 2){
			    	$(".team_1").css('display','inline');
			    	$(".team_2").css('display','inline');
			    }
			});
		</script>
	</body>
</html>
