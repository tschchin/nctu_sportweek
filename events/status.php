<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="final of database">
		<meta name="author" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Status</title>

		<!-- Bootstrap core CSS -->
		
		<link rel="stylesheet" href="./../bootstrap-4.1.1/dist/css/bootstrap.min.css">

		<link rel="stylesheet" href="./../css/home.css">
		<link rel="stylesheet" href="./../css/event.css">
		<link rel="stylesheet" href="./../css/session.css">
		<?php				
			include "./../sql.php";
			session_start();
			if($_SESSION['url'] != ($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']) ) 
				$_SESSION['url'] = ($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			
			$id = $_GET["id"];
			$now = $_GET["now"];
			if(!isset($now))
				$now=0;


			$sql = "SELECT *
					FROM (Registration INNER JOIN team_information on Registration.team_id = team_information.team_id)
					where Registration.event_id = " . $id;

			$ans=[];
			$i=0;
			$result = $conn->query($sql);
			if ($result->num_rows > 0 ) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					$ans[$i] = $row;
					$i++;
				}
				$key = true;
			}
			else {
				$key = false;
				//echo "error<br>";
			}
		?>
	</head>
	<body>
	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark">
				<div class="container-fluid">
					<a class="navbar-brand" href="./../home.php">N C T U &nbsp;&nbsp; S p o r t s</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="nav navbar-nav mr-auto">
							<li class="nav-item">
								<a class="nav-link" href="./../home.php">首頁 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="./../game_match.php">賽程 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link active" href="./../events.php">活動列表 <span class="sr-only">(current)</span></a>
							</li>
							<?php
								$account = $_SESSION['account'];
								$identity = $_SESSION['identity'];
								/*---------登入後---------*/
								if ($account != null)
								{
									if($identity == "admin")
									{?>
									<li class="nav-item active">
										<a class="nav-link" href="./../member_manage.php">成員管理 <span class="sr-only">(current)</span></a>
									</li>
									<?php
									}
									//取出user的name
									$sql = "SELECT name FROM User where user_id = '$account'";
									$result = mysqli_query($conn,$sql);
									$row = @mysqli_fetch_row($result);
									?>
									<li class="nav-item active">
										<a class="nav-link" href="#">Hi, <?php echo $row[0] ?></a>
										<ul class="profile-option">
											<li><a href="./../person_info.php">個人資料</a></li>
											<li><a href="./../logout.php">登出</a></li>
										</ul>
									</li>
								<?php
								}
								/*---------未登入---------*/
								else {?>
									<li class="nav-item active">
										<a class="nav-link " href="./../register.php">註冊 <span class="sr-only">(current)</span></a>
									</li>
									<li class="nav-item active">
										<a class="nav-link " href="./../login.php">登入 <span class="sr-only">(current)</span></a>
									</li>
								<?php
								}?>
						</ul>
					</div>
				</div>
			</nav>
		</header>


		<div class="container event-wrapper event-list">
			<br>
			<a href="./../events.php"><button class="btn btn-primary" style="float:right;">回上頁</button></a>
			<br>
			<h3 class="title" style="text-align:center;">報名狀況:<?php echo $name;?></h3>
			<br>
			<div class="status-form">
				<h3 class="title" style="text-align:center;">點入隊伍名稱可以看到更多資訊:<?php echo $name; ?></h3>
				<?php

					if($key)
					{
						echo "<table class='table'>";
						echo "<tbody>";
						echo "<tr>";
						echo "<th>隊伍名稱</th>";
						echo "<th>該隊人數</th>";
						echo "	</tr>";
						for($n =$now ;$n<$now+6 && $n<count($ans);$n++)
						{
							$temp = explode("_",$ans[$n]["name"]);
							echo "<tr>";
							echo "<td><a href=list.php?id=".$id."&team_id=".$ans[$n]["team_id"]."&team_name=";
							for($i=0;$i<count($temp)-1;$i++)
								echo $temp[$i];	
							echo ">";
							for($i=0;$i<count($temp)-1;$i++)
								echo $temp[$i];	
							echo "</a></td>\n";
							echo "<td>".$ans[$n]["size"]."</td>\n";
							echo "</tr>\n";
						}
						echo "</tbody>\n";				
						echo "</table>\n";

						echo '<h3 class="title" style="text-align:center;">';
						if($now>0)
							echo '<a href=status.php?id='.$id.'&now='.($now-6).'>上一頁</a>';
						if(count($temp)-6>$now)
							echo '<a href=status.php?id='.$id.'&now='.($now+6).'>下一頁</a>';
						echo "</h3>";
					}
					else
					{
						echo '<h3 class="title" style="text-align:center;">';
						echo "還沒有人報名 快來報名<br>";
						echo "</h3>";
					}	
					
				?>
			</div>
		</div>


		<footer class="text-muted">
			<div class="container">
				<p class="float-right">
					<a href="#">Back to top</a>
				</p>
			</div>
		</footer>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		
		<script>window.jQuery || document.write('<script src="./../bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
		<script src="./../bootstrap-4.1.1/assets/js/vendor/holder.min.js"></script>
		<script src="./../bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
		<script src="./../bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
	</body>
</html>
