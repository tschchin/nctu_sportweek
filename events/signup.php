<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="final of database">
		<meta name="author" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Event</title>

		<!-- Bootstrap core CSS -->
		
		<link rel="stylesheet" href="./../bootstrap-4.1.1/dist/css/bootstrap.min.css">

		<link rel="stylesheet" href="./../css/home.css">
		<link rel="stylesheet" href="./../css/event.css">
		<link rel="stylesheet" href="./../css/session.css">
		<?php		
			include "./../sql.php";
			session_start();
			
			$id = $_GET["id"];
			
			$sql = "SELECT name,team_limit,max_team_members,min_team_members
							FROM event
							where id = " . $id
							." limit 1";
			$result = $conn->query($sql);
			if ($result->num_rows > 0 ) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					$name = $row['name'];
					$team_limit = $row['team_limit'];
					$max_team_members = $row['max_team_members'];
					$min_team_members = $row['min_team_members'];
				}
			}
			else {
				echo "error<br>";
			}
			
			$sql = "SELECT count(distinct team_id) as s FROM Registration where event_id = " . $id;
			

			$result = $conn->query($sql);
			if ($result->num_rows > 0 ) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					$team_num = $row['s'];
				}
			}
			else {
				echo "error<br>";
			}
            /*
            $u_name=[];
            $u_id=[];
            $class=[];
            $grade=[];
            $d_name=[];
            */
			$peopleErr = "";
			if($_SESSION['url'] != ($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']) ) 
			{
				$_SESSION['url'] = ($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
				$_SESSION['data'] = array();
			}
			if(isset($_POST['button_count']))
			{

				if(count($_SESSION['data'])!=$max_team_members || $_SESSION['identity']=='admin')
                {
                    if($_POST['people']!="")
                    {
                        for($i=0 ; $i<count($_SESSION['data']) ; $i++)
                            if($_POST['people'] === $_SESSION['data'][$i]['u_id'])
                            {
                                $peopleErr = "用過了";
                                break;
                            }
                        if($peopleErr=="")
                        {
                            $query = 	"SELECT User.name as u_name,User.user_id as u_id, Department.class as class , Department.grade as grade , Department.name as d_name
                                    FROM (User INNER JOIN Department ON User.department_id = Department.id)
                                    WHERE User.user_id = \"" . $_POST['people'] . "\"";
                            $result = $conn->query($query);
                            if ($result->num_rows > 0 ) 
                            {
                                $i = count($_SESSION['data']);
                                $_SESSION['data'][$i]=array();

                                while($row = $result->fetch_assoc())
                                {
                                    $_SESSION['data'][$i]['u_name'] = $row["u_name"];
                                    $_SESSION['data'][$i]['u_id'] = $row["u_id"];
                                    $_SESSION['data'][$i]['class'] = $row["class"];
                                    $_SESSION['data'][$i]['grade'] = $row["grade"];
                                    $_SESSION['data'][$i]['d_name'] = $row["d_name"];
                                }
                            }
                            else
                                $peopleErr = "此使用者不存在";
                        }
                        
                    }
                    else
                        $peopleErr = "請輸入學號";
                }
                else
                    $peopleErr = "已到最大數量";

			}   			
			else  
			{
                for($i=0; $i<count($_SESSION['data']);$i++)
                    if(isset($_POST['reduce_count'.(string)$i]))
                    {
                        array_splice($_SESSION['data'],$i,1);
                    	unset($_POST['reduce_count'.(string)$i]);
                    }
            }   		
            
			$teamname = $_POST["name"];
			$num = 0;
			if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['cancel']))
			{
				header('Location:../events.php'); 
			}
			else if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['submit']) 
			{
				$key = true;

				if(empty($_POST["name"]))
				{
					$nameErr = "這是必填項";
					$key = false;
				}
				else
				{
					$sql = "SELECT * FROM team_information WHERE name='".$teamname ."_".$event_id."'";
					$result = $conn->query($sql);
					if ($result->num_rows > 0 ) 
					{
						$nameErr = "這個名字被用過了";
						$key = false; 
					}
				}
					
				if((count($_SESSION['data']) < $min_team_members) && $_SESSION['identity']!='admin')
				{
					$peopleErr = "輸入的使用者數量不足";
					$key = false;
				}
				if($key)
					for($i=0; $i<count($_SESSION['data']);$i++)
						if($_SESSION['data'][$i]['u_id'] === $_SESSION['account'] ||  $_SESSION['identity']==='admin')
						{
							$key=true;
							$peopleErr = "";
							break;
						}
						else
						{
							$peopleErr = "請不要幫別人報名";
							$key = false;
						}
				if($key)
				{          
					$peopleErr="";    
					if(count($_SESSION['data']) < $min_team_members && $_SESSION['identity']!='admin')
						$peopleErr = "輸入的使用者 有人尚未註冊";
					else
					{
						$check_word = "你的隊伍為" . $_POST["name"] . "成員為：	";
						for($i=0 ; $i<count($_SESSION['data']) ; $i++)
							$check_word .= ($_SESSION['data'][$i]['d_name'] . " " . $_SESSION['data'][$i]['grade'] . " " . $_SESSION['data'][$i]['class'] . " " .$_SESSION['data'][$i]['u_id'] . " " .$_SESSION['data'][$i]['u_name']);
							
						echo $check_word ;
						echo '<script>';
						echo 'if(confirm("' . $check_word . '")) ';
						echo 'window.location.href="make_team.php?';
						echo "name=".$_POST["name"]."&event_id=".$id;
						for($i=0 ; $i<count($_SESSION['data']) ; $i++)
							echo "&id[]=".$_SESSION['data'][$i]['u_id'] ;
						echo '";</script>';
						
						echo $_SESSION["test"] ; 
					}
				}
			}
		?>

	</head>
	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark">
				<div class="container-fluid">
					<a class="navbar-brand" href="./../home.php">N C T U &nbsp;&nbsp; S p o r t s</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="nav navbar-nav mr-auto">
							<li class="nav-item">
								<a class="nav-link" href="./../home.php">首頁 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="./../game_match.php">賽程 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link active" href="./../events.php">活動列表 <span class="sr-only">(current)</span></a>
							</li>
							<?php
								$account = $_SESSION['account'];
								$identity = $_SESSION['identity'];
								/*---------登入後---------*/
								if ($account != null)
								{
									if($identity == "admin")
									{?>
									<li class="nav-item active">
										<a class="nav-link" href="./../member_manage.php">成員管理 <span class="sr-only">(current)</span></a>
									</li>
									<?php
									}
									//取出user的name
									$sql = "SELECT name FROM User where user_id = '$account'";
									$result = mysqli_query($conn,$sql);
									$row = @mysqli_fetch_row($result);
									?>
									<li class="nav-item active">
										<a class="nav-link" href="#">Hi, <?php echo $row[0] ?></a>
										<ul class="profile-option">
											<li><a href="./../person_info.php">個人資料</a></li>
											<li><a href="./../logout.php">登出</a></li>
										</ul>
									</li>
								<?php
								}
								/*---------未登入---------*/
								else {?>
									<li class="nav-item active">
										<a class="nav-link " href="./../register.php">註冊 <span class="sr-only">(current)</span></a>
									</li>
									<li class="nav-item active">
										<a class="nav-link " href="./../login.php">登入 <span class="sr-only">(current)</span></a>
									</li>
								<?php
								}?>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<div class="container event-wrapper event-list">
			<br>
			<a href="./../events.php"><button class="btn btn-primary" style="float:right;">回上頁</button></a>
			<br>
			<br>
			<div class="signup-form">
				<h3 class="title">活動報名:<?php echo $name; ?></h3>
				<table class="table">
					<tbody>
						<tr>
							<th>隊伍上限</th>
							<th>已報名隊伍數量</th>
							<th>隊伍人數上限</th>
							<th>隊伍人數下限</th>
						</tr>
						<tr>
							<td><?php echo $team_limit; ?></td>
							<td><?php echo $team_num; ?></td>
							<td><?php echo $max_team_members; ?></td>
							<td><?php echo $min_team_members; ?></td>
						</tr>
					</tbody>				
				</table>
				<br>
				<div style="text-align:center;">每一項活動的報名截止日期皆在開始前一周的晚上11：59：59。
				<br>
					<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ."?". htmlspecialchars($_SERVER['QUERY_STRING']);?>">  
						隊伍名稱:<br> 
						<span style="text-align:center; color:#e02323; "><?php
						if(empty($nameErr))
							printf('*');
						else
							echo $nameErr;
						?></span>
						<br>
						<input type="text" name="name" value = <?php printf(" '%s' ",$teamname); ?> >
						<br><br>

						隊伍成員:<br>
						<span style="text-align:center; color:#e02323; "><?php
						if(empty($peopleErr))
							printf('請按順序填寫');
						else
							echo $peopleErr;
						?></span>
						<br>
						<table class="table_input">
							<?php
								for($i=0 ; $i < count($_SESSION['data']) ; $i++)
								{
                                    printf("<tr><th>%s</th><th>%s</th>",$_SESSION['data'][$i]['u_id'],$_SESSION['data'][$i]['u_name']);
                                    echo '<th><input type="submit" name="reduce_count'.(string)$i.'" value="移除"></th></tr>';						
                                }
							?>
							
							<tr>
								<th><input class="input" type="text" name="people"  value = <?php printf(" \"%s\" ",$_POST['people'][0]); ?> ><br></th>
								<th><input type="submit" name="button_count" value="增加欄位"></th>
							</tr>
							
						</table>
						<br>
						<?php
							if($team_num<$team_limit || $_SESSION['identity']=='admin')
								echo "<input type='submit' name='submit' value='Submit'>";
							
							echo "<input type='submit' name='cancel' value='取消報名'>";
							echo "</form>";
							
							
						?>
				</div>
			</div>
		</div>


		<footer class="text-muted">
			<div class="container">
				<p class="float-right">
					<a href="#">Back to top</a>
				</p>
			</div>
		</footer>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		
		<script>window.jQuery || document.write('<script src="./../bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
		<script src="./../bootstrap-4.1.1/assets/js/vendor/holder.min.js"></script>
		<script src="./../bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
		<script src="./../bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
	</body>
</html>
