<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="final of database">
		<meta name="author" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Edit</title>

		<!-- Bootstrap core CSS -->

		<link rel="stylesheet" href="./../bootstrap-4.1.1/dist/css/bootstrap.min.css">

		<link rel="stylesheet" href="./../css/home.css">
		<link rel="stylesheet" href="./../css/event.css">
		<link rel="stylesheet" href="./../css/session.css">

		<?php
			include "./../sql.php";
			session_start();			
			if($_SESSION['url'] != ($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']) ) 
				$_SESSION['url'] = ($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			
			$id = $_GET["id"];
			$query = "SELECT * from event where id= ". $id .  " limit 1";
			$result = $conn->query($query);
			
			while($row = $result->fetch_array())
			{
				//print_r($row);
				$name = $row['name'];
				$date = $row['ddate'];
				$sum  = $row['team_limit'];
				$min  = $row['min_team_members'];
				$max  = $row['max_team_members'];
				$rule = $row['rule'];
			}
			$result->close();
			
			// define variables and set to empty values
			$nameErr = $dateErr = $sumErr = $minErr = $maxErr = $ruleErr = "";

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$date = test_input($_POST["date"]);
		
				$sum = test_input($_POST["sum"]);
				if(!is_numeric($sum)){
					$sumErr = "should be an integer";
					$sum = "";
				}
				$rule = test_input($_POST["rule"]);
	
				$min = test_input($_POST["min"]);
				if(!is_numeric($min)){
					$minErr = "should be an integer";
					$min = "";
				}

				$max = test_input($_POST["max"]);
				if(!is_numeric($max)){
					$maxErr = "should be an integer";
					$max = "";
				}

				//check if the input is all filled 
				if ( (!empty($name)) && (!empty($date)) && (!empty($sum)) && (!empty($min)) && (!empty($max)) && (!empty($rule)) )
				{
					//$user->modify_query($id,$date,$sum,$min,$max,$rule);
					$sql = "UPDATE event SET " . " ddate= '" . $date ."', team_limit= " . $sum ." ,rule= '" . $rule ."' ,min_team_members=" . $min ." ,max_team_members= " . $max ." WHERE id= " . $id;
					echo $sql;	
					
					if ($conn->query($sql) === TRUE) {
						//echo "Record updated successfully";
					} else {
						echo "Error updating record: " . $conn->error;
					}	

					header('Location:../events.php');	
				}
			}
	
			function test_input($data) {
				$data = trim($data);
				$data = stripslashes($data);
				$data = htmlspecialchars($data);
				return $data;
			}
		?>
	</head>
	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark">
				<div class="container-fluid">
					<a class="navbar-brand" href="./../home.php">N C T U &nbsp;&nbsp; S p o r t s</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="nav navbar-nav mr-auto">
							<li class="nav-item">
								<a class="nav-link" href="./../home.php">首頁 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="./../game_match.php">賽程 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link active" href="./../events.php">活動列表 <span class="sr-only">(current)</span></a>
							</li>
							<?php
								$account = $_SESSION['account'];
								$identity = $_SESSION['identity'];
								/*---------登入後---------*/
								if ($account != null)
								{
									if($identity == "admin")
									{?>
									<li class="nav-item active">
										<a class="nav-link" href="./../member_manage.php">成員管理 <span class="sr-only">(current)</span></a>
									</li>
									<?php
									}
									//取出user的name
									$sql = "SELECT name FROM User where user_id = '$account'";
									$result = mysqli_query($conn,$sql);
									$row = @mysqli_fetch_row($result);
									?>
									<li class="nav-item active">
										<a class="nav-link" href="#">Hi, <?php echo $row[0] ?></a>
										<ul class="profile-option">
											<li><a href="./../person_info.php">個人資料</a></li>
											<li><a href="./../logout.php">登出</a></li>
										</ul>
									</li>
								<?php
								}
								/*---------未登入---------*/
								else {?>
									<li class="nav-item active">
										<a class="nav-link " href="./../register.php">註冊 <span class="sr-only">(current)</span></a>
									</li>
									<li class="nav-item active">
										<a class="nav-link " href="./../login.php">登入 <span class="sr-only">(current)</span></a>
									</li>
								<?php
								}?>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<div class="container event-wrapper event-list">
			<br>
			<a href="./../events.php"><button class="btn btn-primary" style="float:right;">回上頁</button></a>
			<br>
			<h3 class="title">修改活動:<?php echo $name; ?></h3>
			<br>
			<div class="add-form">	
				<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) . "?id=" . $id;?>">  
					活動日期:<span class="error"> <?php
					if(empty($dateErr))
						printf('請按這以下格式輸入yyyy-mm-dd');
					else
						echo $dateErr;
					?></span>
						<br><input type="text" name="date" value="<?php echo $date; ?>" />
					<br><br>
					隊伍限制:<span class="error"> <?php
			
					if(empty($sumErr))
						printf('請輸入隊伍數量上限');
					else
						echo $sumErr;
					?></span>

						<br><input type="text" name="sum" value="<?php echo $sum; ?>"/>
					<br><br>
		
					<table>
						<tr>
							<th>每隊最小人數: <span class="error"><?php
			
							if(empty($minErr))
								printf('*');
							else
								echo $minErr;

							?></span></th>
							<th>每隊最大人數: <span class="error"><?php
			
							if(empty($maxErr))
								printf('*');
							else
								echo $maxErr;

							?></span></th>
						</tr>
						<tr>
							<th><input type="text" name="min" value="<?php echo $min; ?>"/></th>
							<th><input type="text" name="max" value="<?php echo $max; ?>"/></th>
						</tr>
					</table>

					<br>
					規則:<span class="error"><?php
					if(empty($ruleErr))
						printf('*');
					else
						echo $ruleErr;
					?></span>
						<br><textarea name="rule"> <?php echo $rule; ?></textarea>
					<br><br>
					<table>
					<tr>
						<th>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>
						<th><input type="submit" name="submit" value="save"></th>
						<th><a href="./../events.php"><input type="button" name="cancel" value="cancel"></a></th>
					</tr>
					</table>  
				</form>
			</div>
		</div>

		<footer class="text-muted">
			<div class="container">
				<p class="float-right">
					<a href="#">Back to top</a>
				</p>
			</div>
		</footer>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    	<script>window.jQuery || document.write('<script src="./bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
		<script src="./bootstrap-4.1.1/assets/js/vendor/holder.min.js"></script>
		<script src="./bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
		<script src="./bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
	</body>
</html>
