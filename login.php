<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="final of database">
		<meta name="author" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>登入</title>

		<!-- Bootstrap core CSS -->

		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">-->
		<link rel="stylesheet" href="./bootstrap-4.1.1/dist/css/bootstrap.min.css">

		<link rel="stylesheet" href="./css/home.css">
		<link rel="stylesheet" href="./css/album.css">
		<link rel="stylesheet" href="./css/login.css">
		<!-- Custom styles for this template -->
	</head>

	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark">
				<div class="container-fluid">
					<a class="navbar-brand" href="#">N C T U &nbsp;&nbsp; S p o r t s</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="nav navbar-nav mr-auto">
							<li class="nav-item active">
								<a class="nav-link" href="home.php">首頁 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="game_match.php">賽程<span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="events.php">活動列表 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link " href="./register.php">註冊 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item active">
								<a class="nav-link active" href="login.php">登入 <span class="sr-only">(current)</span></a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<div class="container login-wrapper">
			<h1 style="text-align:center;">登入</h1>

		  <div id="center_block">
		    <form action="session.php" method="post" style="text-align:center">
		    <input type="text" placeholder="學號" name="account" id="text" value="" autocomplete="off"><br><br>
		    <input type="password" placeholder="密碼" name="password" autocomplete="off"><br><br>
		    <input type="submit" name="login" value="登入"><br><br>
		    </form>
		  </div>

		  <p>還沒註冊嗎？ 快來<a href="register.php">註冊</a></p>
		</div>




		<footer class="text-muted">
			<div class="container">
				<p class="float-right">
					<a href="#">Back to top</a>
				</p>
			</div>
		</footer>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    	<script>window.jQuery || document.write('<script src="./bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
		<script src="./bootstrap-4.1.1/assets/js/vendor/holder.min.js"></script>
		<script src="./bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
		<script src="./bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
	</body>
</html>
